var $ = require('jquery');

var TagsScript = {

    run: function(elem, callback, tag_collection){
        try {
            tag_collection.length > 0;
        } catch(e) {
            console.warn('Token Parser: "tags" JSON structure must be provided.');
            return false;
        }

        var elem = $(elem);
        var tag_input_value = '';

        var set_sniffed_tags = function(str, cb) {
            tag_collection.forEach(function(item, index) {
                tag_collection[index].selected = item.needle.test(str);
            });

            cb();
        }

        var render_tags = function() {
            var tags = [];

            tag_collection.forEach(function(item) {
                if (item.selected === true) {
                    tags.push(item.display_name);
                    if (item.associates) {
                        item.associates.forEach(function(associate) {
                            if (tags.indexOf(associate) === -1) {
                                tags.push(associate);
                            }
                        });
                    }
                }
            });
            callback(tags);
            // @todo: render only if tags have changed
            // elem.find('.tags').val(tags.join());
        }

        // Bootstrap self
        elem.find('.name').on('keyup', 
            function(event) {
                set_sniffed_tags(event.target.value, render_tags);
            });
    }

}

module.exports = TagsScript;