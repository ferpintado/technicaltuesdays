/** @jsx React.DOM */
var React = require('react/addons');
var AppActions = require('../../actions/app-actions.js');
var WebApi = require('../../utils/web-api.js');
var DateHelpers = require('../../utils/date-helpers.js');
var CookieHelpers = require('../../utils/cookie-helpers.js');
var StoreWatchMixin = require('../../mixins/StoreWatchMixin.js');
var AppStore = require('../../stores/app-store.js');
var Display = require('../utils/display-when.js');
var TagsScript = require('../../../vendor/token-parser.js');
var TagsComponent = require('../list/tags.js');
var TagCollection = require('../list/tag_collection.js');

function getSentStatus(){
	return {sent: AppStore.getSentStatus()}
}

var SuggestForm = React.createClass({
	mixins: [React.addons.LinkedStateMixin, new StoreWatchMixin(getSentStatus)],
	getInitialState: function() {
		var emailAddress = CookieHelpers.getItem('tt-user-email-address');

		return {
			email: emailAddress || "",
			name: "",
			url: "",
			tags: [],
			existingUser: !!emailAddress
		}
	},
	componentDidMount: function() {
		TagsScript.run(this.getDOMNode(), this.getTags, TagCollection);

	},
	getTags: function(tags){
		var newState = React.addons.update(this.state, {
		    tags: { 
		    	$set: tags 
		    }		  
		});
		this.setState(newState);
	},
	submitForm: function(e){
		e.preventDefault();
		var link = {
			dateCreated: new Date(),
			publishDate: DateHelpers.getNextTuesdayDate(),
			name: this.state.name,
			email: this.state.email,
			url: this.state.url,
			tags : this.state.tags,
			approved: false,
			top_pick: false
		};

		// @todo: this needs moving to a new home
		CookieHelpers.setItem("tt-user-email-address", this.state.email);
		this.setState({existingUser: true});

		WebApi.insertLink(link);
		this.replaceState(this.getInitialState())
	},
	render: function() {
	    var msg;

	    if (this.state.sent){
	    	msg = <p className="alert alert-success">{this.state.sent} Link submitted! Waiting for approval.</p>;
	    }

		return (
			<div>
				<form className="form-inline" onSubmit={this.submitForm}>
					<h2 className="form-title">Submit Your Link</h2>
					<div className="form-group name"><label>Title:</label><input placeholder="Top 10 Workflow Tools" className="form-control name" type="text" valueLink={this.linkState('name')} required /></div>
					<div className="form-group"><label>URL:</label><input placeholder="http://www.tech-articles.com/top-ten-workflow-tools" className="form-control" type="text" valueLink={this.linkState('url')} required /></div>
					<div className={"form-group " + (this.state.existingUser ? 'hide' : '')}><label>E-mail:</label><input placeholder="johnsmith@bestbuycanada.ca" className="form-control" type="text" valueLink={this.linkState('email')} required /></div>
					<TagsComponent tags={this.state.tags}></TagsComponent>
					{msg}
					<button className="btn btn-default" type="submit" value="send">Send</button>
				</form>
			</div>
		);
	}
})

module.exports = SuggestForm;