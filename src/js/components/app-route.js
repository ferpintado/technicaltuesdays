/** @jsx React.DOM */
var React = require('react');
var Router = require('react-router-component');
var TechnicalTuesdays = require('./app.js');
var ApproveLink = require('./manage/approve-link.js');

var Locations = Router.Locations;
var Location = Router.Location; 

var APP = React.createClass({
    render:function(){
      return (
        <div>
          <Locations hash>
            <Location path="/" handler={TechnicalTuesdays} />
            <Location path="/approve/:id" handler={ApproveLink} />
          </Locations>
        </div>

        )
    }
  });

module.exports = APP;
