/** @jsx React.DOM */

var React = require('react');
var StoreWatchMixin = require('../../mixins/StoreWatchMixin.js');
var Link = require('react-router-component').Link;
var WebApi = require('../../utils/web-api.js');
var AppStore = require('../../stores/app-store.js');
var LinkDetails = require('./link-details.js');
var Display = require('../utils/display-when.js');

function getLink(){
  return {link: AppStore.getLink()}
}

var ApproveLink = React.createClass({
	mixins: [new StoreWatchMixin(getLink)],
	getInitialState: function(){
		return {
			approved: false,
			render: false 
		}
	},
	componentDidMount:function(){
		WebApi.getLink(this.props.id);
	}, 
	approveAction: function(e){
		e.preventDefault();
		WebApi.approveLink(this.props.id);
	},
	componentWillUpdate: function(nextProps, nextState) {
      nextState.approved = nextState.link.approved;
      nextState.render = true;
    },
	render: function() {
		var link = this.state.link;
		return (
			<div>
				<h1>Approve Link</h1>
				<Display when={this.state.render && !this.state.approved}>
					<LinkDetails link={link} />
					<a className="btn btn-success" onClick={this.approveAction}>Approve</a>
				</Display>
				<Display when={this.state.approved}>
					<p className="alert alert-success">Link approved</p>
					<Link href={'/'} className="btn btn-default">Back to archive</Link>
				</Display>
			</div>
		);
	}
});

module.exports = ApproveLink;