/** @jsx React.DOM */

var React = require('react');

var LinkDetails = React.createClass({
	render: function() {
		var link = this.props.link;		
		if (typeof link.tags == "undefined"){
			link = {tags: []}
		}
		return (
			<div className="LinkDetails">
				<p><strong>Title:</strong> {link.name} </p>
				<p><strong>URL:</strong> { link.url }</p>
				<p><strong>E-mail:</strong> { link.email }</p>
				<p><strong>Tags:</strong> { link.tags.join() }</p>
			</div>
		);
	}
});

module.exports = LinkDetails;