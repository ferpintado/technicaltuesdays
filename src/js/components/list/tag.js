/** @jsx React.DOM */
var React = require('react');

var Tag = React.createClass({
	render: function() {
		return (
			<li className="tag">{this.props.tagName}</li>
		)
	}
})

module.exports = Tag;