/** @jsx React.DOM */
var React = require('react');
var Tag = require('./tag.js');

var TagsComponent = React.createClass({
	render: function() {
		var tags = this.props.tags;
		return (
			<ul className="tags-list">
				{tags.map(function(tag, i){		
					return <Tag key={i} tagName={tag}></Tag>
				})}
			</ul>
		);
	}

})

module.exports = TagsComponent;