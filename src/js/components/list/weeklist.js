/** @jsx React.DOM */
var React = require('react');
var LinkComponent = require('./link.js');

var WeekList = React.createClass({
	render: function() {
		var list = this.props.list;
		return (
			<div className="month-wrapper">
				<h2>{this.props.publishDate}</h2>
				<ul className="links-list">
					{list.reverse().map(function( data, i){
						return (
							<LinkComponent key={i} data={data}></LinkComponent>
						)
					})}
				</ul>
			</div>
		);
	}
})

module.exports = WeekList;