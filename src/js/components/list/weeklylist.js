/** @jsx React.DOM */
var React = require('react');
var _ = require('lodash');
var WeekList = require('./weeklist.js');
var moment = require('moment');


var WeeklyList = React.createClass({
	render: function() {
		_.map(this.props.list, function(item){
			item.publishDate = moment(item.publishDate).format('MMM D, YYYY')
		})
		var list = _.groupBy(this.props.list, 'publishDate');
		var i = -1;
		return (
			<div>
			 {
			 	_.mapValues(list, function(week, publishDate) {			 		
			 		i++;
			 		return <WeekList key={i} publishDate={publishDate} list={ week }></WeekList>
			 	})
			 }
			 </div>
		);
	}
});

module.exports = WeeklyList;