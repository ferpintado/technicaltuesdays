/** @jsx React.DOM */
var React = require('react');
var TagsComponent = require('./tags.js');

var LinkComponent = React.createClass({
	render: function() {
		var link = this.props.data;
		return (
			<li className="links-entry">
				<a href={link.url} target="_blank">{link.name}</a>
				<br />
				<TagsComponent tags={link.tags}></TagsComponent>
			</li>
		);
	}
})

module.exports = LinkComponent;