module.exports = [{
    id: "react",
    needle: new RegExp(/react/i),
    display_name: "react.js",
    selected: false
}, {
    id: "git",
    needle: new RegExp(/git/i),
    display_name: "git",
    selected: false
}, {
    id: "bisect",
    needle: new RegExp(/bisect/i),
    display_name: "bisect",
    selected: false,
    associates: ["git"]
}, {
    id: "testing",
    needle: new RegExp(/testing/i),
    display_name: "testing",
    selected: false
},{
    id: "angular",
    needle: new RegExp(/angular/i),
    display_name: "angular.js",
    selected: false
}, {
    id: "gulp",
    needle: new RegExp(/gulp/i),
    display_name: "gulp.js",
    selected: false,
    associates: ["node.js"]
}, {
    id: "ux",
    needle: new RegExp(/\sux(\s|$)/i),
    display_name: "testing",
    selected: false
}, {
    id: "flux",
    needle: new RegExp(/flux/i),
    display_name: "flux",
    selected: false
}, {
    id: "js",
    needle: new RegExp(/js|javascript/i),
    display_name: "javascript",
    selected: false
}, {
    id: "node",
    needle: new RegExp(/node/i),
    display_name: "node.js",
    selected: false
}, {
    id: ".net",
    needle: new RegExp(/\.net|asp\s/i),
    display_name: ".net",
    selected: false
}, {
    id: "java",
    needle: new RegExp(/java\s/i),
    display_name: "java",
    selected: false
}, {
    id: "c#",
    needle: new RegExp(/c\#/i),
    display_name: "c#",
    selected: false
}, {
    id: "agile",
    needle: new RegExp(/agile/i),
    display_name: "agile",
    selected: false
}, {
    id: "automation",
    needle: new RegExp(/automat/i),
    display_name: "automation",
    selected: false
}, {
    id: "es6",
    needle: new RegExp(/es6/i),
    display_name: "es6",
    selected: false,
    associates: ["javascript"]
}, {
    id: "accessibility",
    needle: new RegExp(/accessibility/i),
    display_name: "accessibility",
    selected: false
}, {
    id: "chrome",
    needle: new RegExp(/chrome/i),
    display_name: "chrome",
    selected: false
}, {
    id: "gitlab",
    needle: new RegExp(/gitlab|git\slab/i),
    display_name: "git-lab",
    selected: false
}, {
    id: "css",
    needle: new RegExp(/css/i),
    display_name: "css",
    selected: false
}, {
    id: "html",
    needle: new RegExp(/html/i),
    display_name: "html",
    selected: false
}, {
    id: "scrum",
    needle: new RegExp(/scrum/i),
    display_name: "scrum",
    selected: false,
    associates: ["agile"]
}, {
    id: "cli",
    needle: new RegExp(/\scli(\s|$)/i),
    display_name: "cli",
    selected: false
}, {
    id: "xamarin",
    needle: new RegExp(/xamarin/i),
    display_name: "xamarin",
    selected: false,
    associates: [".net"]
}, {
    id: "npm",
    needle: new RegExp(/npm/i),
    display_name: "npm",
    selected: false,
    associates: ["node.js"]
}, {
    id: "nginx",
    needle: new RegExp(/nginx/i),
    display_name: "nginx",
    selected: false
}, {
    id: "video",
    needle: new RegExp(/youtube/i),
    display_name: "video",
    selected: false
}, {
    id: "egghead.io",
    needle: new RegExp(/egghead/i),
    display_name: "egghead.io",
    selected: false
}, {
    id: "typescript",
    needle: new RegExp(/typescript/i),
    display_name: "typescript",
    selected: false
}, {
    id: "protractor",
    needle: new RegExp(/protractor/i),
    display_name: "protractor",
    selected: false
}, {
    id: "apple",
    needle: new RegExp(/apple/i),
    display_name: "apple",
    selected: false
}, {
    id: "events",
    needle: new RegExp(/event/i),
    display_name: "events",
    selected: false
}, {
    id: "isomorphic",
    needle: new RegExp(/isomorphic/i),
    display_name: "isomorphic",
    selected: false
}, {
    id: "sass",
    needle: new RegExp(/sass/i),
    display_name: "sass",
    selected: false
}, {
    id: "markdown",
    needle: new RegExp(/markdown/i),
    display_name: "markdown",
    selected: false
}, {
    id: "seo",
    needle: new RegExp(/\sseo/i),
    display_name: "seo",
    selected: false
}, {
    id: "express",
    needle: new RegExp(/express/i),
    display_name: "express.js",
    associates: ["node.js"],
    selected: false
}, {
    id: "mongoDB",
    needle: new RegExp(/mongo/i),
    display_name: "mongoDB",
    selected: false
}, {
    id: "uml",
    needle: new RegExp(/uml/i),
    display_name: "uml",
    selected: false
}, {
    id: "svg",
    needle: new RegExp(/svg/i),
    display_name: "svg",
    selected: false
}, {
    id: "sprites",
    needle: new RegExp(/sprit/i),
    display_name: "sprites",
    associates: ["css", "performance"],
    selected: false
}];