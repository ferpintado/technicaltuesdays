/** @jsx React.DOM */
var React = require('react');
var WeeklyList = require('./list/weeklylist.js');
var SuggestForm = require('./form/form.js');
var StoreWatchMixin = require('../mixins/StoreWatchMixin.js');
var AppStore = require('../stores/app-store.js');
var WebApi = require('../utils/web-api.js');



function getLinks(){
  return {list: AppStore.getLinks()}
}

var TechnicalTuesdays = React.createClass({
  mixins: [new StoreWatchMixin(getLinks)],
  componentWillMount: function(){
    WebApi.getLinks();
  },
  render: function() {
    return (
      <div className="container">
        <h1>Technical Tuesdays</h1>
        <SuggestForm/>
        <WeeklyList list={ this.state.list } />
      </div>
    );
  }
})

module.exports = TechnicalTuesdays;