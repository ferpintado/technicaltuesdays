/** @jsx React.DOM */
var React = require('react');

var Display = React.createClass({
	render: function() {
		var component;
		if (this.props.when){
			component = this.props.children
		}
		return (
			<div>
			{component}
			</div>
		);
	}
});

module.exports = Display;