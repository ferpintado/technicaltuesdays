var $ = require('jquery');
var AppActions = require('../actions/app-actions.js');
var Mailer = require('./mailer.js');

var MONGODB = "https://api.mongolab.com/api/1"
var MONGO_APIKEY = "Vdz35gaMBV76-3-FkZtXFG776lKjidS1";
var db = 'technicaltuesdays'
var collection = 'links';

var endpoint = MONGODB + "/databases/" + db + "/collections/" + collection + "?apiKey=" + MONGO_APIKEY

var WebApi = {
	getLinks: function(){
		$.ajax({
			url: endpoint,
			type : "GET",
			data : 's={"publishDate": -1}&q={"approved": true}', 
			contentType: "application/json"
		}).done(function(links){
			AppActions.getLinks(links);
		});
	},
	insertLink: function(link){
		$.ajax({
			url: endpoint,
			data: JSON.stringify(link),
			type : "POST",
			contentType: "application/json"
		}).done(function(link){
			AppActions.submitLink(link);
			Mailer.sendMail(link);
		})

	},
	getLink: function(id){
		$.ajax({
			url: endpoint,
			type : "GET",
			data : 'q={"_id": {"$oid": "'+ id +'"}}&fo=true',
			contentType: "application/json"
		}).done(function(link){
			AppActions.setLink(link);
		});		
	},
	approveLink: function(id){
		var data = JSON.stringify( { "$set" : { "approved" : true } } ) ;
		var url = MONGODB + "/databases/" + db + "/collections/" + collection + "/" + id + "?apiKey=" + MONGO_APIKEY;

		$.ajax({
			url: url,
			type : "PUT",
			data : data,
			contentType: "application/json"
		}).done(function(link){
			AppActions.setLink(link);
		});			
	}
}

module.exports = WebApi;
