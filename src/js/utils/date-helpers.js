var moment = require('moment');

var DateHelpers = {
	getNextTuesdayDate: function(){
		var diff = 0;
		var today = moment();
		var dayOfWeekInt = today.day();

		switch (dayOfWeekInt) {
			case 0:
				diff = 2;
				break;
			case 1:
				diff = 1;
				break;
			case 2:
				diff = 0;
				break;
			case 3:
				diff = 6;
				break;
			case 4:
				diff = 5;
				break;
			case 5:
				diff = 4;
				break;
			case 6:
				diff = 3;
				break;
			default:
				console.warn('Bad day of week');
		}

		return today.add(diff, 'days').format();
	}
}

module.exports = DateHelpers;