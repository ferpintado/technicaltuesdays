var appConfig = require('../../../config.json');
var $ = require('jquery');
var MANDRILL_APIKEY = "nDo5DCWitAeLFB85Uk07rQ";

// var sendTo = [
// 	{
// 		'email': 'fpintadovazquez@bestbuycanada.ca',
// 		'name': 'Fernando Pintado',
// 		'type': 'to'
// 	}
// ]

var sendTo = [
	{
		'email': 'chrsaund@bestbuycanada.ca',
		'name': 'Chris Saunders',
		'type': 'to'
	}
]

var fromEmail = 'fpintadovazquez@bestbuycanada.ca';
var subject = "Link submitted"
var fromName = "Technical Tuesdays";
var url = appConfig.domain + "/#/approve/"

function createHTML(link){
	var html =
		"<h1>Technical Tuesdays</h1>"
	+	"<h2>A new link has been submitted</h2>"
	+	"<p><strong>Title:</strong> " + link.name + "</p>"
	+	"<p><strong>URL:</strong> <a href='" + link.url + "'>" + link.url + "</a></p>"
	+	"<p><strong>E-mail:</strong> " + link.email + "</p>"
	+	"<p><strong>Tags:</strong> " + link.tags.join() + "</p>"
	+	"<p><a href='" + url + link._id.$oid +"'>Approve link</a></p>";
	return html;
}

module.exports = {
	sendMail: function(link){
		$.ajax({
		  type: "POST",
		  url: "https://mandrillapp.com/api/1.0/messages/send.json",
		  data: {
		    'key': MANDRILL_APIKEY,
		    'message': {
		      'from_email': fromEmail,
		      "from_name": fromName,
		      'to': sendTo,
		      'autotext': 'true',
		      'subject': subject,
		      'html': createHTML(link)
		    }
		  }
		 }).done(function(response) {
		   // console.log(response); 
		 });
	}
}



