var AppConstants = require('../constants/app-constants.js');
var AppDispatcher = require('../dispatchers/app-dispatcher.js');

var AppActions = {
	getLinks: function(links){
		AppDispatcher.handleViewAction({
		  actionType: AppConstants.GET_LINKS,
		  links: links
		})
	},

	addLink:function(link){
		AppDispatcher.handleViewAction({
		  actionType: AppConstants.ADD_LINK,
		  link: link
		})
	},

	setLink: function(link){
		AppDispatcher.handleViewAction({
		  actionType: AppConstants.SET_LINK,
		  link: link
		})		
	},

	submitLink: function(link){
		AppDispatcher.handleViewAction({
		  actionType: AppConstants.SUBMIT_LINK,
		  link: link
		})
	}

}

module.exports = AppActions;
