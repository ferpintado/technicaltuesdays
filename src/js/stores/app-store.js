var AppDispatcher = require('../dispatchers/app-dispatcher');
var AppConstants = require('../constants/app-constants');
var merge = require('react/lib/merge');
var moment = require('moment');
var _ = require('lodash');
var EventEmitter = require('events').EventEmitter;

var CHANGE_EVENT = "change";

var _links = [];
var _link = {};
var _linkSent = false;
var _tags = [];

function _addLink(link){
  _links.push(link);
}

function _setLinkCollection(links){ 
  _links = links;
  _tags = [];
  _.forEach(links, function(link, i){
    _.forEach(link.tags, function(tag, j){
      _tags.push(tag);
    })
  })
}

function _linkSubmitted(){
  _linkSent = true;
  setTimeout(function(){
    _linkSent = false;
    AppStore.emitChange();
  }, 5000)
}

var AppStore = merge(EventEmitter.prototype, {
  emitChange:function(){
    this.emit(CHANGE_EVENT)
  },

  addChangeListener:function(callback){
    this.on(CHANGE_EVENT, callback)
  },

  removeChangeListener:function(callback){
    this.removeListener(CHANGE_EVENT, callback)
  },

  getLinks: function(){
    return _links;
  },

  getNumLinks:function(){
    return _links.length
  },

  getLink: function()  {
    return _link;
  },

  getSentStatus: function(){
    return _linkSent;
  },

  getTags: function(){
    return _tags;
  },

  dispatcherIndex:AppDispatcher.register(function(payload){
    var action = payload.action; // this is our action from handleViewAction
    switch(action.actionType){
      case AppConstants.ADD_LINK:
        _addLink(payload.action.link);
      break;
      case AppConstants.GET_LINKS:
        _setLinkCollection(payload.action.links);
      break;
      case AppConstants.SET_LINK:
        _link = payload.action.link
      break;
      case AppConstants.SUBMIT_LINK:
        _linkSubmitted();
      break;

    }
    AppStore.emitChange();

    return true;
  })
})

module.exports = AppStore;
