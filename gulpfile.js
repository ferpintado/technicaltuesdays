var gulp = require('gulp');
var browserify = require('gulp-browserify');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var reload = browserSync.reload;

gulp.task('browserify', function() {
    gulp.src('src/js/main.js')
      .pipe(browserify({transform: 'reactify'}))
      .pipe(concat('main.js'))
      .pipe(gulp.dest('dist/js'))
      .pipe(reload({stream: true}));
});

gulp.task('copy', function() {
    gulp.src('src/index.html')
      .pipe(gulp.dest('dist'));
    gulp.src('src/assets/**/*.*')
      .pipe(gulp.dest('dist/assets'))
      .pipe(reload({stream: true}));
});

// CSS tasks
gulp.task('sass', function(){
	gulp.src('src/scss/*.scss')
	.pipe(concat('styles.scss'))
	.pipe(sass())
	.pipe(gulp.dest('dist/css'))
	.pipe(reload({stream: true}));
});

// Server and watch tasks
gulp.task('serve', function() {

    browserSync({
        server: {
            baseDir: "./dist"
        }
    });

    gulp.watch("src/scss/*.scss", ['sass']);
    gulp.watch("src/js/*.js", ['browserify', 'copy']);
});

gulp.task('default',['browserify', 'copy', 'sass']);

gulp.task('watch', function() {
    gulp.watch('src/**/*.*', ['default']);
});
