# Technical Tuesdays
> High-visibility knowledge-sharing platform

## Objectives
### Primary
* Foster knowledge dispersal (inside and across teams).
* Persist otherwise transient knowledge-sharing, whilst requiring the absolute minimum of user effort.
* Build an archive of valuable learning resources.

### Secondary
* Facilitate innovation by providing a non-customer-facing internal platform, to sandbox new technologies and concepts.
* Identify "learning beacons" a.k.a pro-active knowledge-sharers, via analytics. Why is this important? Training efforts can be channelled through such individuals, who have proven themselves to be inclined to spreading correct knowledge.

## Release History
* **v0.2.0** - 2015-03-07
* * User email address stored as cookie
* * UX tweaks to approval emails
* * Automatically schedule links under up-coming week's publish
* * Minor UI style tweaks
* * Copy changes
* * Added more tags to collection
* **v0.1.0** - 2015-03-06
* * Automatic tag detection
* * Approval flow

## Authors
* Chris Saunders / [chrsaund@bestbuycanada.ca](mailto:chrsaund@bestbuycanada.ca)
* Fernando Pintado / [fpintadovazquez@bestbuycanada.ca](mailto:fpintadovazquez@bestbuycanada.ca)

## License
TBD